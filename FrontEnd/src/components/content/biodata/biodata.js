import React from 'react'
import apiconfig from '../../../configs/api.configs.json'
import axios from 'axios'
import {Link} from 'react-router-dom'
import ViewBiodata from'./viewBiodata'
import ViewBiodata2 from'./viewbiodata2'
import EditBiodata from'./editBiodata'

class biodata extends React.Component{
    constructor(props){
        super(props)
        this.state={
            biodata:[],
            currentBiodata:{},
            viewBiodata:false,
            view2Biodata:false
            
        }
        this.viewModalHandler = this.viewModalHandler.bind(this)
        this.closeModalHandler = this.closeModalHandler.bind(this)
    }
    viewModalHandler(kodebiodata){
        let tmp = {}
        this.state.biodata.map((row)=>{
            if(kodebiodata == row.id){
                tmp = row
            }
        })
        this.setState({
            currentBiodata : tmp,
            viewBiodata:true
        })
        this.getListBiodata()
    }
    closeModalHandler(){
        this.setState({
            viewBiodata : false    
        })
        this.getListBiodata()
        
        
    }

  

    getListBiodata(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.BIODATA,
            method:"get",
            header:{
                "Authorization": token
            }
        }
        axios(option).then((response)=>{
            this.setState({
                biodata: response.data.message
            })
        }).catch((error)=>{
            alert(error)
        })
    }
    componentDidMount(){
        this.getListBiodata()
    }
    render(){
        return(
            <div>
                 <ViewBiodata
                view = {this.state.viewBiodata}
                piew = {this.viewModalHandler}
                closeModalHandler = {this.closeModalHandler}
                popupHandler = {this.popupHandler}
                biodata = {this.state.currentBiodata}
                getlist = {this.getListBiodata}
                go = {this.componentDidMount}/>
            <div class="container">
                <h4>
            <form class="form-inline ml-3">
              <div class="input-group input-group-sm">
              List Biodata <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"/>
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </form> </h4>

                <table style={{borderStyle:'insert'}}id="mytable" class="table table-bordered table-striped">
                 <thead>
                     <tr>
                         </tr></thead> 
                         <tbody>
                             {
                                this.state.biodata.map((row,x)=>
                                <tr> <Link to='#'>
                                   <td onClick={()=>{this.viewModalHandler(row.id)}}> {row.fullname}</td> 
                                        </Link>
                                            <td>
                                    </td>
                                </tr>)
                             }</tbody>  
                </table>
            </div>
        </div>
        )
    }

}
export default biodata