import axios from 'axios'
import apiconfig from '../configs/api.configs.json'


const API = {

    Bio :async (id
        ,created_by
        ,created_on
        ,modified_by
        ,modified_on
        ,deleted_by
        ,deleted_on
        ,is_delete
        ,fullname
        ,nick_name
        ,pob
        ,dob
        ,gender
        ,religion_id
        ,high
        ,weight
        ,nationality
        ,ethnic
        ,hobby
        ,identity_type_id
        ,identity_no
        ,email
        ,phone_number1
        ,phone_number2
        ,parent_phone_number
        ,child_sequence
        ,how_many_brothers
        ,marital_status_id
        ,addrbook_id
        ,token
        ,expired_token
        ,marriage_year
        ,company_id
        ,is_process
        ,is_complete
        )=>{
        let token=localStorage.getItem(apiconfig.LS.TOKEN)
        let option={
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.COMPANY,
            method: "GET",
            headers: {
                "Authorization": token
            },
            data:{
                id : id,
                created_by           :created_by,
                created_on       :created_on,
                modified_by       :modified_by,
                modified_on        :modified_on,
                deleted_by         : deleted_by,
                deleted_on         :deleted_on,
                is_delete       :is_delete,
                fullname       :fullname,
                nick_name         :nick_name,
                pob            :pob,
                dob                 :dob,
                gender              :gender,
                religion_id          :religion_id,
                high                :high,
                weight           :weight,
                nationality         :nationality,
                ethnic           :ethnic,
                hobby             :hobby,
                identity_type_id  :identity_type_id,
                identity_no       :identity_no,
                email            :email,
                phone_number1        :phone_number1,   
                phone_number2        :phone_number2,  
                parent_phone_number  :parent_phone_number,      
                child_sequence       :child_sequence,     
                how_many_brothers    :how_many_brothers,  
                marital_status_id    :marital_status_id,      
                addrbook_id       :addrbook_id,
                token           :token,
                expired_token      :expired_token,
                marriage_year        :marriage_year,
                company_id           :company_id,
                is_process         :is_process,
                is_complete          :is_complete
                
            }
        }
        try {
            let result = await axios(option)
            return result.data
        } catch(error){
            return error.response.data
        }
    }

}
export default API