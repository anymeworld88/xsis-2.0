PGDMP         "        
        x            xsis20    12.2    12.2 n   E           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            F           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            G           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            H           1262    24960    xsis20    DATABASE     �   CREATE DATABASE xsis20 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Indonesian_Indonesia.1252' LC_CTYPE = 'Indonesian_Indonesia.1252';
    DROP DATABASE xsis20;
                postgres    false            �            1259    25368 
   x_addrbook    TABLE     l  CREATE TABLE public.x_addrbook (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    is_locked boolean NOT NULL,
    attempt integer NOT NULL,
    email character varying(100) NOT NULL,
    abuid character varying(50) NOT NULL,
    abpwd character varying(50) NOT NULL,
    fp_token character varying(100),
    fp_expired_date timestamp without time zone,
    fp_counter integer NOT NULL
);
    DROP TABLE public.x_addrbook;
       public         heap    postgres    false            �            1259    25366    x_addrbook_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_addrbook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_addrbook_id_seq;
       public          postgres    false    209            I           0    0    x_addrbook_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_addrbook_id_seq OWNED BY public.x_addrbook.id;
          public          postgres    false    208            �            1259    25419 	   x_address    TABLE     O  CREATE TABLE public.x_address (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    address1 character varying(1000),
    postal_code1 character varying(20),
    rt1 character varying(5),
    rw1 character varying(5),
    kelurahan1 character varying(100),
    kecamatan1 character varying(100),
    region1 character varying(100),
    address2 character varying(1000),
    postal_code2 character varying(20),
    rt2 character varying(5),
    rw2 character varying(5),
    kelurahan2 character varying(100),
    kecamatan2 character varying(100),
    region2 character varying(100)
);
    DROP TABLE public.x_address;
       public         heap    postgres    false            �            1259    25417    x_address_id_seq    SEQUENCE     y   CREATE SEQUENCE public.x_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.x_address_id_seq;
       public          postgres    false    221            J           0    0    x_address_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.x_address_id_seq OWNED BY public.x_address.id;
          public          postgres    false    220            �            1259    25376 	   x_biodata    TABLE     �  CREATE TABLE public.x_biodata (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    fullname character varying(255) NOT NULL,
    nick_name character varying(100) NOT NULL,
    pob character varying(100) NOT NULL,
    dob date NOT NULL,
    gender boolean NOT NULL,
    religion_id bigint NOT NULL,
    high integer,
    weight integer,
    nationality character varying(100),
    ethnic character varying(50),
    hobby character varying(255),
    identity_type_id bigint NOT NULL,
    identity_no character varying(50) NOT NULL,
    email character varying(100) NOT NULL,
    phone_number1 character varying(50) NOT NULL,
    phone_number2 character varying(50),
    parent_phone_number character varying(50) NOT NULL,
    child_sequence character varying(5),
    how_many_brothers character varying(5),
    marital_status_id bigint NOT NULL,
    addrbook_id bigint,
    token character varying(10),
    expired_token date,
    marriage_year character varying(10),
    company_id bigint NOT NULL,
    is_process boolean,
    is_complete boolean
);
    DROP TABLE public.x_biodata;
       public         heap    postgres    false            �            1259    25551    x_biodata_attachment    TABLE     �  CREATE TABLE public.x_biodata_attachment (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    file_name character varying(100),
    file_path character varying(1000),
    notes character varying(1000),
    is_photo boolean
);
 (   DROP TABLE public.x_biodata_attachment;
       public         heap    postgres    false            �            1259    25549    x_biodata_attachment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_biodata_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.x_biodata_attachment_id_seq;
       public          postgres    false    245            K           0    0    x_biodata_attachment_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.x_biodata_attachment_id_seq OWNED BY public.x_biodata_attachment.id;
          public          postgres    false    244            �            1259    25374    x_biodata_id_seq    SEQUENCE     y   CREATE SEQUENCE public.x_biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.x_biodata_id_seq;
       public          postgres    false    211            L           0    0    x_biodata_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.x_biodata_id_seq OWNED BY public.x_biodata.id;
          public          postgres    false    210            �            1259    25562 	   x_catatan    TABLE     �  CREATE TABLE public.x_catatan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    title character varying(100),
    note_type_id bigint,
    notes character varying(1000)
);
    DROP TABLE public.x_catatan;
       public         heap    postgres    false            �            1259    25560    x_catatan_id_seq    SEQUENCE     y   CREATE SEQUENCE public.x_catatan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.x_catatan_id_seq;
       public          postgres    false    247            M           0    0    x_catatan_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.x_catatan_id_seq OWNED BY public.x_catatan.id;
          public          postgres    false    246            #           1259    25745    x_certification_type    TABLE     �  CREATE TABLE public.x_certification_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);
 (   DROP TABLE public.x_certification_type;
       public         heap    postgres    false            "           1259    25743    x_certification_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_certification_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.x_certification_type_id_seq;
       public          postgres    false    291            N           0    0    x_certification_type_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.x_certification_type_id_seq OWNED BY public.x_certification_type.id;
          public          postgres    false    290                       1259    25707    x_client    TABLE     �  CREATE TABLE public.x_client (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(100) NOT NULL,
    user_client_name character varying(100) NOT NULL,
    ero bigint NOT NULL,
    user_email character varying(100) NOT NULL
);
    DROP TABLE public.x_client;
       public         heap    postgres    false                       1259    25705    x_client_id_seq    SEQUENCE     x   CREATE SEQUENCE public.x_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.x_client_id_seq;
       public          postgres    false    281            O           0    0    x_client_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.x_client_id_seq OWNED BY public.x_client.id;
          public          postgres    false    280                       1259    25691 	   x_company    TABLE     �  CREATE TABLE public.x_company (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
    DROP TABLE public.x_company;
       public         heap    postgres    false                       1259    25689    x_company_id_seq    SEQUENCE     y   CREATE SEQUENCE public.x_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.x_company_id_seq;
       public          postgres    false    277            P           0    0    x_company_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.x_company_id_seq OWNED BY public.x_company.id;
          public          postgres    false    276                       1259    25635    x_education_level    TABLE     �  CREATE TABLE public.x_education_level (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    delete_by bigint,
    delete_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
 %   DROP TABLE public.x_education_level;
       public         heap    postgres    false                       1259    25633    x_education_level_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_education_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.x_education_level_id_seq;
       public          postgres    false    263            Q           0    0    x_education_level_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.x_education_level_id_seq OWNED BY public.x_education_level.id;
          public          postgres    false    262                       1259    25699 
   x_employee    TABLE     �  CREATE TABLE public.x_employee (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    is_idle boolean,
    is_ero boolean,
    is_user_client boolean,
    ero_email character varying(100)
);
    DROP TABLE public.x_employee;
       public         heap    postgres    false                       1259    25697    x_employee_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_employee_id_seq;
       public          postgres    false    279            R           0    0    x_employee_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_employee_id_seq OWNED BY public.x_employee.id;
          public          postgres    false    278            '           1259    25759    x_employee_leave    TABLE     �  CREATE TABLE public.x_employee_leave (
    id bigint NOT NULL,
    created_by bigint,
    created_on timestamp without time zone,
    modified_by bigint NOT NULL,
    modified_on timestamp without time zone NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on timestamp without time zone NOT NULL,
    is_delete boolean,
    employee_id bigint,
    regular_quota integer,
    annual_collective_leave integer,
    leave_already_taken integer
);
 $   DROP TABLE public.x_employee_leave;
       public         heap    postgres    false            &           1259    25757    x_employee_leave_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_employee_leave_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.x_employee_leave_id_seq;
       public          postgres    false    295            S           0    0    x_employee_leave_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.x_employee_leave_id_seq OWNED BY public.x_employee_leave.id;
          public          postgres    false    294            %           1259    25751    x_employee_training    TABLE     >  CREATE TABLE public.x_employee_training (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    employee_id bigint NOT NULL,
    training_id bigint NOT NULL,
    training_organizer_id bigint,
    training_date timestamp without time zone,
    training_type_id bigint,
    certification_type_id bigint,
    status character varying(15) NOT NULL
);
 '   DROP TABLE public.x_employee_training;
       public         heap    postgres    false            $           1259    25749    x_employee_training_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_employee_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.x_employee_training_id_seq;
       public          postgres    false    293            T           0    0    x_employee_training_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.x_employee_training_id_seq OWNED BY public.x_employee_training.id;
          public          postgres    false    292                       1259    25651    x_family_relation    TABLE     �  CREATE TABLE public.x_family_relation (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    delete_by bigint,
    delete_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100),
    family_tree_type_id bigint
);
 %   DROP TABLE public.x_family_relation;
       public         heap    postgres    false            
           1259    25649    x_family_relation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_family_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.x_family_relation_id_seq;
       public          postgres    false    267            U           0    0    x_family_relation_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.x_family_relation_id_seq OWNED BY public.x_family_relation.id;
          public          postgres    false    266            	           1259    25643    x_family_tree_type    TABLE     �  CREATE TABLE public.x_family_tree_type (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    delete_by bigint,
    delete_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
 &   DROP TABLE public.x_family_tree_type;
       public         heap    postgres    false                       1259    25641    x_family_tree_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_family_tree_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.x_family_tree_type_id_seq;
       public          postgres    false    265            V           0    0    x_family_tree_type_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.x_family_tree_type_id_seq OWNED BY public.x_family_tree_type.id;
          public          postgres    false    264            �            1259    25403    x_identity_type    TABLE     �  CREATE TABLE public.x_identity_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
 #   DROP TABLE public.x_identity_type;
       public         heap    postgres    false            �            1259    25401    x_identity_type_id_seq    SEQUENCE        CREATE SEQUENCE public.x_identity_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.x_identity_type_id_seq;
       public          postgres    false    217            W           0    0    x_identity_type_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.x_identity_type_id_seq OWNED BY public.x_identity_type.id;
          public          postgres    false    216            �            1259    25540 
   x_keahlian    TABLE     �  CREATE TABLE public.x_keahlian (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    skill_name character varying(100),
    skill_level_id bigint,
    notes character varying(1000)
);
    DROP TABLE public.x_keahlian;
       public         heap    postgres    false            �            1259    25538    x_keahlian_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_keahlian_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_keahlian_id_seq;
       public          postgres    false    243            X           0    0    x_keahlian_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_keahlian_id_seq OWNED BY public.x_keahlian.id;
          public          postgres    false    242            �            1259    25529 
   x_keluarga    TABLE     E  CREATE TABLE public.x_keluarga (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    family_tree_type_id bigint,
    family_relation_id bigint,
    name character varying(100),
    gender boolean NOT NULL,
    dob date,
    education_level_id bigint,
    job character varying(100),
    notes character varying(1000)
);
    DROP TABLE public.x_keluarga;
       public         heap    postgres    false            �            1259    25527    x_keluarga_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_keluarga_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_keluarga_id_seq;
       public          postgres    false    241            Y           0    0    x_keluarga_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_keluarga_id_seq OWNED BY public.x_keluarga.id;
          public          postgres    false    240            �            1259    25496    x_keterangan_tambahan    TABLE       CREATE TABLE public.x_keterangan_tambahan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    emergency_contact_name character varying(100),
    emergency_contact_phone character varying(50),
    expected_salary character varying(20),
    is_negotiable boolean,
    start_working character varying(100),
    is_ready_to_outoftown boolean,
    is_apply_other_place boolean,
    apply_place character varying(100),
    selection_phase character varying(100),
    is_ever_badly_sick boolean,
    disease_name character varying(100),
    disease_time character varying(100),
    is_ever_psychotest boolean,
    psychotest_needs character varying(100),
    psychotest_time character varying(100),
    requirementes_required character varying(500),
    other_notes character varying(1000)
);
 )   DROP TABLE public.x_keterangan_tambahan;
       public         heap    postgres    false            �            1259    25494    x_keterangan_tambahan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_keterangan_tambahan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.x_keterangan_tambahan_id_seq;
       public          postgres    false    235            Z           0    0    x_keterangan_tambahan_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.x_keterangan_tambahan_id_seq OWNED BY public.x_keterangan_tambahan.id;
          public          postgres    false    234            )           1259    25767    x_leave_name    TABLE     �  CREATE TABLE public.x_leave_name (
    id bigint NOT NULL,
    created_by bigint,
    created_on timestamp without time zone,
    modified_by bigint NOT NULL,
    modified_on timestamp without time zone NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on timestamp without time zone NOT NULL,
    is_delete boolean,
    leave_name_id bigint,
    s_start date,
    e_end date,
    reason character varying(255),
    leave_contact character varying(50),
    leave_address character varying(255)
);
     DROP TABLE public.x_leave_name;
       public         heap    postgres    false            (           1259    25765    x_leave_name_id_seq    SEQUENCE     |   CREATE SEQUENCE public.x_leave_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.x_leave_name_id_seq;
       public          postgres    false    297            [           0    0    x_leave_name_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.x_leave_name_id_seq OWNED BY public.x_leave_name.id;
          public          postgres    false    296            +           1259    25778    x_leave_request    TABLE     �  CREATE TABLE public.x_leave_request (
    id bigint NOT NULL,
    created_by bigint,
    created_on timestamp without time zone,
    modified_by bigint NOT NULL,
    modified_on timestamp without time zone NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on timestamp without time zone NOT NULL,
    is_delete boolean,
    s_start date,
    e_end date,
    reason character varying(255),
    leave_contact character varying(50),
    leave_address character varying(255)
);
 #   DROP TABLE public.x_leave_request;
       public         heap    postgres    false            *           1259    25776    x_leave_request_id_seq    SEQUENCE        CREATE SEQUENCE public.x_leave_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.x_leave_request_id_seq;
       public          postgres    false    299            \           0    0    x_leave_request_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.x_leave_request_id_seq OWNED BY public.x_leave_request.id;
          public          postgres    false    298            �            1259    25411    x_marital_status    TABLE     J  CREATE TABLE public.x_marital_status (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
 $   DROP TABLE public.x_marital_status;
       public         heap    postgres    false            �            1259    25409    x_marital_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_marital_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.x_marital_status_id_seq;
       public          postgres    false    219            ]           0    0    x_marital_status_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.x_marital_status_id_seq OWNED BY public.x_marital_status.id;
          public          postgres    false    218            �            1259    25360    x_menu_access    TABLE     y  CREATE TABLE public.x_menu_access (
    id bigint NOT NULL,
    menutree_id bigint NOT NULL,
    role_id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL
);
 !   DROP TABLE public.x_menu_access;
       public         heap    postgres    false            �            1259    25358    x_menu_access_id_seq    SEQUENCE     }   CREATE SEQUENCE public.x_menu_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.x_menu_access_id_seq;
       public          postgres    false    207            ^           0    0    x_menu_access_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.x_menu_access_id_seq OWNED BY public.x_menu_access.id;
          public          postgres    false    206            �            1259    25352 
   x_menutree    TABLE     N  CREATE TABLE public.x_menutree (
    id bigint NOT NULL,
    title character varying(50) NOT NULL,
    menu_image_url character varying(100),
    menu_icon character varying(100),
    menu_order integer,
    menu_level integer,
    menu_parent bigint NOT NULL,
    menu_url character varying(100),
    menu_type character varying(10),
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL
);
    DROP TABLE public.x_menutree;
       public         heap    postgres    false            �            1259    25350    x_menutree_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_menutree_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_menutree_id_seq;
       public          postgres    false    205            _           0    0    x_menutree_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_menutree_id_seq OWNED BY public.x_menutree.id;
          public          postgres    false    204                       1259    25667    x_note_type    TABLE     �  CREATE TABLE public.x_note_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
    DROP TABLE public.x_note_type;
       public         heap    postgres    false                       1259    25665    x_note_type_id_seq    SEQUENCE     {   CREATE SEQUENCE public.x_note_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.x_note_type_id_seq;
       public          postgres    false    271            `           0    0    x_note_type_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.x_note_type_id_seq OWNED BY public.x_note_type.id;
          public          postgres    false    270            �            1259    25573    x_online_test    TABLE     
  CREATE TABLE public.x_online_test (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    period_code character varying(50),
    period integer,
    test_date date,
    expired_test date,
    user_access character varying(10),
    status character varying(50)
);
 !   DROP TABLE public.x_online_test;
       public         heap    postgres    false            �            1259    25581    x_online_test_detail    TABLE     �  CREATE TABLE public.x_online_test_detail (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    online_test_id bigint,
    test_type_id bigint,
    test_order integer
);
 (   DROP TABLE public.x_online_test_detail;
       public         heap    postgres    false            �            1259    25579    x_online_test_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_online_test_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.x_online_test_detail_id_seq;
       public          postgres    false    251            a           0    0    x_online_test_detail_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.x_online_test_detail_id_seq OWNED BY public.x_online_test_detail.id;
          public          postgres    false    250            �            1259    25571    x_online_test_id_seq    SEQUENCE     }   CREATE SEQUENCE public.x_online_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.x_online_test_id_seq;
       public          postgres    false    249            b           0    0    x_online_test_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.x_online_test_id_seq OWNED BY public.x_online_test.id;
          public          postgres    false    248            �            1259    25518    x_organisasi    TABLE     ;  CREATE TABLE public.x_organisasi (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    name character varying(100),
    "position" character varying(100),
    entry_year character varying(10),
    exit_year character varying(10),
    responsibility character varying(100),
    notes character varying(1000)
);
     DROP TABLE public.x_organisasi;
       public         heap    postgres    false            �            1259    25516    x_organisasi_id_seq    SEQUENCE     |   CREATE SEQUENCE public.x_organisasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.x_organisasi_id_seq;
       public          postgres    false    239            c           0    0    x_organisasi_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.x_organisasi_id_seq OWNED BY public.x_organisasi.id;
          public          postgres    false    238            �            1259    25485    x_pe_referensi    TABLE     �  CREATE TABLE public.x_pe_referensi (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    name character varying(100),
    "position" character varying(100),
    address_phone character varying(1000),
    relation character varying(100)
);
 "   DROP TABLE public.x_pe_referensi;
       public         heap    postgres    false            �            1259    25483    x_pe_referensi_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.x_pe_referensi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.x_pe_referensi_id_seq;
       public          postgres    false    233            d           0    0    x_pe_referensi_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.x_pe_referensi_id_seq OWNED BY public.x_pe_referensi.id;
          public          postgres    false    232                       1259    25715    x_placement    TABLE     �  CREATE TABLE public.x_placement (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    client_id bigint NOT NULL,
    employee_id bigint NOT NULL,
    is_placement_active boolean NOT NULL
);
    DROP TABLE public.x_placement;
       public         heap    postgres    false                       1259    25713    x_placement_id_seq    SEQUENCE     {   CREATE SEQUENCE public.x_placement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.x_placement_id_seq;
       public          postgres    false    283            e           0    0    x_placement_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.x_placement_id_seq OWNED BY public.x_placement.id;
          public          postgres    false    282            �            1259    25395 
   x_religion    TABLE     �  CREATE TABLE public.x_religion (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
    DROP TABLE public.x_religion;
       public         heap    postgres    false            �            1259    25393    x_religion_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_religion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_religion_id_seq;
       public          postgres    false    215            f           0    0    x_religion_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_religion_id_seq OWNED BY public.x_religion.id;
          public          postgres    false    214            �            1259    25589    x_rencana_jadwal    TABLE     �  CREATE TABLE public.x_rencana_jadwal (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    schedule_code character varying(20),
    schedule_date date,
    "time" character varying(10),
    ro bigint,
    tro bigint,
    schedule_type_id bigint,
    location character varying(100),
    other_ro_tro character varying(100),
    notes character varying(1000),
    is_automatic_mail boolean,
    sent_date date,
    status character varying(50)
);
 $   DROP TABLE public.x_rencana_jadwal;
       public         heap    postgres    false            �            1259    25600    x_rencana_jadwal_detail    TABLE     �  CREATE TABLE public.x_rencana_jadwal_detail (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    rencana_jadwal_id bigint NOT NULL,
    biodata_id bigint NOT NULL
);
 +   DROP TABLE public.x_rencana_jadwal_detail;
       public         heap    postgres    false            �            1259    25598    x_rencana_jadwal_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_rencana_jadwal_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.x_rencana_jadwal_detail_id_seq;
       public          postgres    false    255            g           0    0    x_rencana_jadwal_detail_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.x_rencana_jadwal_detail_id_seq OWNED BY public.x_rencana_jadwal_detail.id;
          public          postgres    false    254            �            1259    25587    x_rencana_jadwal_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_rencana_jadwal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.x_rencana_jadwal_id_seq;
       public          postgres    false    253            h           0    0    x_rencana_jadwal_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.x_rencana_jadwal_id_seq OWNED BY public.x_rencana_jadwal.id;
          public          postgres    false    252            1           1259    25840    x_resource_project    TABLE     H  CREATE TABLE public.x_resource_project (
    id integer NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    client_id bigint NOT NULL,
    location character varying(100),
    department character varying(100),
    pic_name character varying(100),
    project_name character varying(100) NOT NULL,
    start_project date NOT NULL,
    end_project date NOT NULL,
    project_role character varying(255) NOT NULL,
    project_phase character varying(255) NOT NULL,
    project_description character varying(255) NOT NULL,
    project_technology character varying(255) NOT NULL,
    main_task character varying(255) NOT NULL
);
 &   DROP TABLE public.x_resource_project;
       public         heap    postgres    false            0           1259    25838    x_resource_project_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_resource_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.x_resource_project_id_seq;
       public          postgres    false    305            i           0    0    x_resource_project_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.x_resource_project_id_seq OWNED BY public.x_resource_project.id;
          public          postgres    false    304            �            1259    25430    x_riwayat_pekerjaan    TABLE     @  CREATE TABLE public.x_riwayat_pekerjaan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    company_name character varying(100),
    city character varying(50),
    country character varying(50),
    join_year character varying(10),
    join_month character varying(10),
    resign_year character varying(10),
    resign_month character varying(10),
    last_position character varying(100),
    income character varying(20),
    is_it_related boolean,
    about_job character varying(1000),
    exit_reason character varying(500),
    notes character varying(5000)
);
 '   DROP TABLE public.x_riwayat_pekerjaan;
       public         heap    postgres    false            �            1259    25428    x_riwayat_pekerjaan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_riwayat_pekerjaan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.x_riwayat_pekerjaan_id_seq;
       public          postgres    false    223            j           0    0    x_riwayat_pekerjaan_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.x_riwayat_pekerjaan_id_seq OWNED BY public.x_riwayat_pekerjaan.id;
          public          postgres    false    222            �            1259    25463    x_riwayat_pelatihan    TABLE     �  CREATE TABLE public.x_riwayat_pelatihan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    training_name character varying(100),
    organizer character varying(50),
    training_year character varying(10),
    training_month character varying(10),
    training_duration integer,
    time_period_id bigint,
    city character varying(50),
    country character varying(50),
    notes character varying(1000)
);
 '   DROP TABLE public.x_riwayat_pelatihan;
       public         heap    postgres    false            �            1259    25461    x_riwayat_pelatihan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_riwayat_pelatihan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.x_riwayat_pelatihan_id_seq;
       public          postgres    false    229            k           0    0    x_riwayat_pelatihan_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.x_riwayat_pelatihan_id_seq OWNED BY public.x_riwayat_pelatihan.id;
          public          postgres    false    228            �            1259    25452    x_riwayat_pendidikan    TABLE        CREATE TABLE public.x_riwayat_pendidikan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    school_name character varying(100),
    city character varying(50),
    country character varying(50),
    education_level_id bigint,
    entry_year character varying(10),
    graduation_year character varying(10),
    major character varying(1000),
    gpa double precision,
    notes character varying(1000),
    orders integer,
    judul_ta character varying(255),
    deskripsi_ta character varying(5000)
);
 (   DROP TABLE public.x_riwayat_pendidikan;
       public         heap    postgres    false            �            1259    25450    x_riwayat_pendidikan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_riwayat_pendidikan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.x_riwayat_pendidikan_id_seq;
       public          postgres    false    227            l           0    0    x_riwayat_pendidikan_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.x_riwayat_pendidikan_id_seq OWNED BY public.x_riwayat_pendidikan.id;
          public          postgres    false    226            �            1259    25441    x_riwayat_proyek    TABLE     �  CREATE TABLE public.x_riwayat_proyek (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    riwayat_pekerjaan_id bigint NOT NULL,
    start_year character varying(10),
    start_month character varying(10),
    project_name character varying(50),
    project_duration integer,
    time_period_id bigint,
    client character varying(100),
    project_position character varying(100),
    description character varying(1000)
);
 $   DROP TABLE public.x_riwayat_proyek;
       public         heap    postgres    false            �            1259    25439    x_riwayat_proyek_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_riwayat_proyek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.x_riwayat_proyek_id_seq;
       public          postgres    false    225            m           0    0    x_riwayat_proyek_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.x_riwayat_proyek_id_seq OWNED BY public.x_riwayat_proyek.id;
          public          postgres    false    224            �            1259    25344    x_role    TABLE     �  CREATE TABLE public.x_role (
    id bigint NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL
);
    DROP TABLE public.x_role;
       public         heap    postgres    false            �            1259    25342    x_role_id_seq    SEQUENCE     v   CREATE SEQUENCE public.x_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.x_role_id_seq;
       public          postgres    false    203            n           0    0    x_role_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.x_role_id_seq OWNED BY public.x_role.id;
          public          postgres    false    202                       1259    25683    x_schedule_type    TABLE     �  CREATE TABLE public.x_schedule_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
 #   DROP TABLE public.x_schedule_type;
       public         heap    postgres    false                       1259    25681    x_schedule_type_id_seq    SEQUENCE        CREATE SEQUENCE public.x_schedule_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.x_schedule_type_id_seq;
       public          postgres    false    275            o           0    0    x_schedule_type_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.x_schedule_type_id_seq OWNED BY public.x_schedule_type.id;
          public          postgres    false    274            �            1259    25474    x_sertifikasi    TABLE     w  CREATE TABLE public.x_sertifikasi (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    certificate_name character varying(200),
    publisher character varying(100),
    valid_start_year character varying(10),
    valid_start_month character varying(10),
    until_year character varying(10),
    until_month character varying(10),
    notes character varying(1000)
);
 !   DROP TABLE public.x_sertifikasi;
       public         heap    postgres    false            �            1259    25472    x_sertifikasi_id_seq    SEQUENCE     }   CREATE SEQUENCE public.x_sertifikasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.x_sertifikasi_id_seq;
       public          postgres    false    231            p           0    0    x_sertifikasi_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.x_sertifikasi_id_seq OWNED BY public.x_sertifikasi.id;
          public          postgres    false    230                       1259    25659    x_skill_level    TABLE     �  CREATE TABLE public.x_skill_level (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    delete_by bigint,
    delete_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
 !   DROP TABLE public.x_skill_level;
       public         heap    postgres    false                       1259    25657    x_skill_level_id_seq    SEQUENCE     }   CREATE SEQUENCE public.x_skill_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.x_skill_level_id_seq;
       public          postgres    false    269            q           0    0    x_skill_level_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.x_skill_level_id_seq OWNED BY public.x_skill_level.id;
          public          postgres    false    268            �            1259    25507    x_sumber_loker    TABLE     �  CREATE TABLE public.x_sumber_loker (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    vacancy_source character varying(20),
    candidate_type character varying(10),
    event_name character varying(100),
    career_center_name character varying(100),
    referrer_name character varying(100),
    referrer_phone character varying(20),
    referrer_email character varying(100),
    other_source character varying(100),
    last_income character varying(20),
    apply_date date
);
 "   DROP TABLE public.x_sumber_loker;
       public         heap    postgres    false            �            1259    25505    x_sumber_loker_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.x_sumber_loker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.x_sumber_loker_id_seq;
       public          postgres    false    237            r           0    0    x_sumber_loker_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.x_sumber_loker_id_seq OWNED BY public.x_sumber_loker.id;
          public          postgres    false    236                       1259    25675    x_test_type    TABLE     �  CREATE TABLE public.x_test_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
    DROP TABLE public.x_test_type;
       public         heap    postgres    false                       1259    25673    x_test_type_id_seq    SEQUENCE     {   CREATE SEQUENCE public.x_test_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.x_test_type_id_seq;
       public          postgres    false    273            s           0    0    x_test_type_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.x_test_type_id_seq OWNED BY public.x_test_type.id;
          public          postgres    false    272                       1259    25627    x_time_period    TABLE     �  CREATE TABLE public.x_time_period (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    delete_by bigint,
    delete_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(100)
);
 !   DROP TABLE public.x_time_period;
       public         heap    postgres    false                       1259    25625    x_time_period_id_seq    SEQUENCE     }   CREATE SEQUENCE public.x_time_period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.x_time_period_id_seq;
       public          postgres    false    261            t           0    0    x_time_period_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.x_time_period_id_seq OWNED BY public.x_time_period.id;
          public          postgres    false    260            -           1259    25789    x_timesheet    TABLE     s  CREATE TABLE public.x_timesheet (
    id bigint NOT NULL,
    created_by bigint,
    created_on timestamp without time zone,
    modified_by bigint NOT NULL,
    modified_on timestamp without time zone NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on timestamp without time zone NOT NULL,
    is_delete boolean,
    status character varying(15),
    placement_id bigint,
    timesheet_date date,
    s_start character varying(5),
    e_end character varying(5),
    overtime boolean NOT NULL,
    start_ot character varying(5) NOT NULL,
    end_ot character varying(5) NOT NULL,
    activity character varying(255),
    user_approval character varying(50),
    submitted_on timestamp without time zone,
    approved_on timestamp without time zone,
    ero_status character varying(50),
    sent_on timestamp without time zone,
    collected_on timestamp without time zone
);
    DROP TABLE public.x_timesheet;
       public         heap    postgres    false            /           1259    25797    x_timesheet_assessment    TABLE     �  CREATE TABLE public.x_timesheet_assessment (
    id bigint NOT NULL,
    created_by bigint,
    created_on timestamp without time zone,
    modified_by bigint NOT NULL,
    modified_on timestamp without time zone NOT NULL,
    deleted_by bigint NOT NULL,
    deleted_on timestamp without time zone NOT NULL,
    is_delete boolean,
    year integer,
    month integer,
    placement_id bigint,
    target_result integer,
    competency integer,
    discipline integer
);
 *   DROP TABLE public.x_timesheet_assessment;
       public         heap    postgres    false            .           1259    25795    x_timesheet_assessment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_timesheet_assessment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.x_timesheet_assessment_id_seq;
       public          postgres    false    303            u           0    0    x_timesheet_assessment_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.x_timesheet_assessment_id_seq OWNED BY public.x_timesheet_assessment.id;
          public          postgres    false    302            ,           1259    25787    x_timesheet_id_seq    SEQUENCE     {   CREATE SEQUENCE public.x_timesheet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.x_timesheet_id_seq;
       public          postgres    false    301            v           0    0    x_timesheet_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.x_timesheet_id_seq OWNED BY public.x_timesheet.id;
          public          postgres    false    300                       1259    25723 
   x_training    TABLE     �  CREATE TABLE public.x_training (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);
    DROP TABLE public.x_training;
       public         heap    postgres    false                       1259    25721    x_training_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_training_id_seq;
       public          postgres    false    285            w           0    0    x_training_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_training_id_seq OWNED BY public.x_training.id;
          public          postgres    false    284                       1259    25731    x_training_organizer    TABLE     �  CREATE TABLE public.x_training_organizer (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    name character varying(100) NOT NULL,
    notes character varying(100) NOT NULL
);
 (   DROP TABLE public.x_training_organizer;
       public         heap    postgres    false                       1259    25729    x_training_organizer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_training_organizer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.x_training_organizer_id_seq;
       public          postgres    false    287            x           0    0    x_training_organizer_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.x_training_organizer_id_seq OWNED BY public.x_training_organizer.id;
          public          postgres    false    286            !           1259    25739    x_training_type    TABLE     �  CREATE TABLE public.x_training_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);
 #   DROP TABLE public.x_training_type;
       public         heap    postgres    false                        1259    25737    x_training_type_id_seq    SEQUENCE        CREATE SEQUENCE public.x_training_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.x_training_type_id_seq;
       public          postgres    false    289            y           0    0    x_training_type_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.x_training_type_id_seq OWNED BY public.x_training_type.id;
          public          postgres    false    288                       1259    25608 
   x_undangan    TABLE     K  CREATE TABLE public.x_undangan (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    schedule_type_id bigint,
    invitation_date date,
    invitation_code character varying(20),
    "time" character varying(10),
    ro bigint,
    tro bigint,
    other_ro_tro character varying(100),
    location character varying(100),
    status character varying(50)
);
    DROP TABLE public.x_undangan;
       public         heap    postgres    false                       1259    25616    x_undangan_detail    TABLE     �  CREATE TABLE public.x_undangan_detail (
    id bigint NOT NULL,
    create_by bigint NOT NULL,
    create_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    delete_by bigint,
    delete_on timestamp without time zone,
    is_delete boolean NOT NULL,
    undangan_id bigint NOT NULL,
    biodata_id bigint NOT NULL,
    notes character varying(1000)
);
 %   DROP TABLE public.x_undangan_detail;
       public         heap    postgres    false                       1259    25614    x_undangan_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.x_undangan_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.x_undangan_detail_id_seq;
       public          postgres    false    259            z           0    0    x_undangan_detail_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.x_undangan_detail_id_seq OWNED BY public.x_undangan_detail.id;
          public          postgres    false    258                        1259    25606    x_undangan_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_undangan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_undangan_id_seq;
       public          postgres    false    257            {           0    0    x_undangan_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_undangan_id_seq OWNED BY public.x_undangan.id;
          public          postgres    false    256            �            1259    25387 
   x_userrole    TABLE     v  CREATE TABLE public.x_userrole (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    addrbook_id bigint NOT NULL,
    role_id bigint NOT NULL
);
    DROP TABLE public.x_userrole;
       public         heap    postgres    false            �            1259    25385    x_userrole_id_seq    SEQUENCE     z   CREATE SEQUENCE public.x_userrole_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.x_userrole_id_seq;
       public          postgres    false    213            |           0    0    x_userrole_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.x_userrole_id_seq OWNED BY public.x_userrole.id;
          public          postgres    false    212            �           2604    25371    x_addrbook id    DEFAULT     n   ALTER TABLE ONLY public.x_addrbook ALTER COLUMN id SET DEFAULT nextval('public.x_addrbook_id_seq'::regclass);
 <   ALTER TABLE public.x_addrbook ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208    209            �           2604    25422    x_address id    DEFAULT     l   ALTER TABLE ONLY public.x_address ALTER COLUMN id SET DEFAULT nextval('public.x_address_id_seq'::regclass);
 ;   ALTER TABLE public.x_address ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    220    221            �           2604    25814    x_biodata id    DEFAULT     l   ALTER TABLE ONLY public.x_biodata ALTER COLUMN id SET DEFAULT nextval('public.x_biodata_id_seq'::regclass);
 ;   ALTER TABLE public.x_biodata ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    211    211            �           2604    25554    x_biodata_attachment id    DEFAULT     �   ALTER TABLE ONLY public.x_biodata_attachment ALTER COLUMN id SET DEFAULT nextval('public.x_biodata_attachment_id_seq'::regclass);
 F   ALTER TABLE public.x_biodata_attachment ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    244    245    245            �           2604    25565    x_catatan id    DEFAULT     l   ALTER TABLE ONLY public.x_catatan ALTER COLUMN id SET DEFAULT nextval('public.x_catatan_id_seq'::regclass);
 ;   ALTER TABLE public.x_catatan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    247    246    247            �           2604    25748    x_certification_type id    DEFAULT     �   ALTER TABLE ONLY public.x_certification_type ALTER COLUMN id SET DEFAULT nextval('public.x_certification_type_id_seq'::regclass);
 F   ALTER TABLE public.x_certification_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    290    291    291            �           2604    25710    x_client id    DEFAULT     j   ALTER TABLE ONLY public.x_client ALTER COLUMN id SET DEFAULT nextval('public.x_client_id_seq'::regclass);
 :   ALTER TABLE public.x_client ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    280    281    281            �           2604    25694    x_company id    DEFAULT     l   ALTER TABLE ONLY public.x_company ALTER COLUMN id SET DEFAULT nextval('public.x_company_id_seq'::regclass);
 ;   ALTER TABLE public.x_company ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    277    276    277            �           2604    25638    x_education_level id    DEFAULT     |   ALTER TABLE ONLY public.x_education_level ALTER COLUMN id SET DEFAULT nextval('public.x_education_level_id_seq'::regclass);
 C   ALTER TABLE public.x_education_level ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    262    263    263            �           2604    25702    x_employee id    DEFAULT     n   ALTER TABLE ONLY public.x_employee ALTER COLUMN id SET DEFAULT nextval('public.x_employee_id_seq'::regclass);
 <   ALTER TABLE public.x_employee ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    279    278    279            �           2604    25762    x_employee_leave id    DEFAULT     z   ALTER TABLE ONLY public.x_employee_leave ALTER COLUMN id SET DEFAULT nextval('public.x_employee_leave_id_seq'::regclass);
 B   ALTER TABLE public.x_employee_leave ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    295    294    295            �           2604    25754    x_employee_training id    DEFAULT     �   ALTER TABLE ONLY public.x_employee_training ALTER COLUMN id SET DEFAULT nextval('public.x_employee_training_id_seq'::regclass);
 E   ALTER TABLE public.x_employee_training ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    292    293    293            �           2604    25654    x_family_relation id    DEFAULT     |   ALTER TABLE ONLY public.x_family_relation ALTER COLUMN id SET DEFAULT nextval('public.x_family_relation_id_seq'::regclass);
 C   ALTER TABLE public.x_family_relation ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    267    266    267            �           2604    25646    x_family_tree_type id    DEFAULT     ~   ALTER TABLE ONLY public.x_family_tree_type ALTER COLUMN id SET DEFAULT nextval('public.x_family_tree_type_id_seq'::regclass);
 D   ALTER TABLE public.x_family_tree_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    265    264    265            �           2604    25406    x_identity_type id    DEFAULT     x   ALTER TABLE ONLY public.x_identity_type ALTER COLUMN id SET DEFAULT nextval('public.x_identity_type_id_seq'::regclass);
 A   ALTER TABLE public.x_identity_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216    217            �           2604    25543    x_keahlian id    DEFAULT     n   ALTER TABLE ONLY public.x_keahlian ALTER COLUMN id SET DEFAULT nextval('public.x_keahlian_id_seq'::regclass);
 <   ALTER TABLE public.x_keahlian ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    242    243    243            �           2604    25532    x_keluarga id    DEFAULT     n   ALTER TABLE ONLY public.x_keluarga ALTER COLUMN id SET DEFAULT nextval('public.x_keluarga_id_seq'::regclass);
 <   ALTER TABLE public.x_keluarga ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    241    240    241            �           2604    25499    x_keterangan_tambahan id    DEFAULT     �   ALTER TABLE ONLY public.x_keterangan_tambahan ALTER COLUMN id SET DEFAULT nextval('public.x_keterangan_tambahan_id_seq'::regclass);
 G   ALTER TABLE public.x_keterangan_tambahan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    234    235    235            �           2604    25770    x_leave_name id    DEFAULT     r   ALTER TABLE ONLY public.x_leave_name ALTER COLUMN id SET DEFAULT nextval('public.x_leave_name_id_seq'::regclass);
 >   ALTER TABLE public.x_leave_name ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    296    297    297            �           2604    25781    x_leave_request id    DEFAULT     x   ALTER TABLE ONLY public.x_leave_request ALTER COLUMN id SET DEFAULT nextval('public.x_leave_request_id_seq'::regclass);
 A   ALTER TABLE public.x_leave_request ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    299    298    299            �           2604    25414    x_marital_status id    DEFAULT     z   ALTER TABLE ONLY public.x_marital_status ALTER COLUMN id SET DEFAULT nextval('public.x_marital_status_id_seq'::regclass);
 B   ALTER TABLE public.x_marital_status ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    219    219            �           2604    25363    x_menu_access id    DEFAULT     t   ALTER TABLE ONLY public.x_menu_access ALTER COLUMN id SET DEFAULT nextval('public.x_menu_access_id_seq'::regclass);
 ?   ALTER TABLE public.x_menu_access ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    206    207    207            �           2604    25355    x_menutree id    DEFAULT     n   ALTER TABLE ONLY public.x_menutree ALTER COLUMN id SET DEFAULT nextval('public.x_menutree_id_seq'::regclass);
 <   ALTER TABLE public.x_menutree ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            �           2604    25670    x_note_type id    DEFAULT     p   ALTER TABLE ONLY public.x_note_type ALTER COLUMN id SET DEFAULT nextval('public.x_note_type_id_seq'::regclass);
 =   ALTER TABLE public.x_note_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    271    270    271            �           2604    25576    x_online_test id    DEFAULT     t   ALTER TABLE ONLY public.x_online_test ALTER COLUMN id SET DEFAULT nextval('public.x_online_test_id_seq'::regclass);
 ?   ALTER TABLE public.x_online_test ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    249    248    249            �           2604    25584    x_online_test_detail id    DEFAULT     �   ALTER TABLE ONLY public.x_online_test_detail ALTER COLUMN id SET DEFAULT nextval('public.x_online_test_detail_id_seq'::regclass);
 F   ALTER TABLE public.x_online_test_detail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    251    250    251            �           2604    25521    x_organisasi id    DEFAULT     r   ALTER TABLE ONLY public.x_organisasi ALTER COLUMN id SET DEFAULT nextval('public.x_organisasi_id_seq'::regclass);
 >   ALTER TABLE public.x_organisasi ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    239    238    239            �           2604    25488    x_pe_referensi id    DEFAULT     v   ALTER TABLE ONLY public.x_pe_referensi ALTER COLUMN id SET DEFAULT nextval('public.x_pe_referensi_id_seq'::regclass);
 @   ALTER TABLE public.x_pe_referensi ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    233    232    233            �           2604    25718    x_placement id    DEFAULT     p   ALTER TABLE ONLY public.x_placement ALTER COLUMN id SET DEFAULT nextval('public.x_placement_id_seq'::regclass);
 =   ALTER TABLE public.x_placement ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    283    282    283            �           2604    25398    x_religion id    DEFAULT     n   ALTER TABLE ONLY public.x_religion ALTER COLUMN id SET DEFAULT nextval('public.x_religion_id_seq'::regclass);
 <   ALTER TABLE public.x_religion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    215    215            �           2604    25592    x_rencana_jadwal id    DEFAULT     z   ALTER TABLE ONLY public.x_rencana_jadwal ALTER COLUMN id SET DEFAULT nextval('public.x_rencana_jadwal_id_seq'::regclass);
 B   ALTER TABLE public.x_rencana_jadwal ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    252    253    253            �           2604    25603    x_rencana_jadwal_detail id    DEFAULT     �   ALTER TABLE ONLY public.x_rencana_jadwal_detail ALTER COLUMN id SET DEFAULT nextval('public.x_rencana_jadwal_detail_id_seq'::regclass);
 I   ALTER TABLE public.x_rencana_jadwal_detail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    254    255    255            �           2604    25843    x_resource_project id    DEFAULT     ~   ALTER TABLE ONLY public.x_resource_project ALTER COLUMN id SET DEFAULT nextval('public.x_resource_project_id_seq'::regclass);
 D   ALTER TABLE public.x_resource_project ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    305    304    305            �           2604    25433    x_riwayat_pekerjaan id    DEFAULT     �   ALTER TABLE ONLY public.x_riwayat_pekerjaan ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_pekerjaan_id_seq'::regclass);
 E   ALTER TABLE public.x_riwayat_pekerjaan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    223    223            �           2604    25466    x_riwayat_pelatihan id    DEFAULT     �   ALTER TABLE ONLY public.x_riwayat_pelatihan ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_pelatihan_id_seq'::regclass);
 E   ALTER TABLE public.x_riwayat_pelatihan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    228    229    229            �           2604    25455    x_riwayat_pendidikan id    DEFAULT     �   ALTER TABLE ONLY public.x_riwayat_pendidikan ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_pendidikan_id_seq'::regclass);
 F   ALTER TABLE public.x_riwayat_pendidikan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226    227            �           2604    25444    x_riwayat_proyek id    DEFAULT     z   ALTER TABLE ONLY public.x_riwayat_proyek ALTER COLUMN id SET DEFAULT nextval('public.x_riwayat_proyek_id_seq'::regclass);
 B   ALTER TABLE public.x_riwayat_proyek ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    225    225            �           2604    25347 	   x_role id    DEFAULT     f   ALTER TABLE ONLY public.x_role ALTER COLUMN id SET DEFAULT nextval('public.x_role_id_seq'::regclass);
 8   ALTER TABLE public.x_role ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            �           2604    25686    x_schedule_type id    DEFAULT     x   ALTER TABLE ONLY public.x_schedule_type ALTER COLUMN id SET DEFAULT nextval('public.x_schedule_type_id_seq'::regclass);
 A   ALTER TABLE public.x_schedule_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    274    275    275            �           2604    25477    x_sertifikasi id    DEFAULT     t   ALTER TABLE ONLY public.x_sertifikasi ALTER COLUMN id SET DEFAULT nextval('public.x_sertifikasi_id_seq'::regclass);
 ?   ALTER TABLE public.x_sertifikasi ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    231    230    231            �           2604    25662    x_skill_level id    DEFAULT     t   ALTER TABLE ONLY public.x_skill_level ALTER COLUMN id SET DEFAULT nextval('public.x_skill_level_id_seq'::regclass);
 ?   ALTER TABLE public.x_skill_level ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    268    269    269            �           2604    25510    x_sumber_loker id    DEFAULT     v   ALTER TABLE ONLY public.x_sumber_loker ALTER COLUMN id SET DEFAULT nextval('public.x_sumber_loker_id_seq'::regclass);
 @   ALTER TABLE public.x_sumber_loker ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    236    237    237            �           2604    25678    x_test_type id    DEFAULT     p   ALTER TABLE ONLY public.x_test_type ALTER COLUMN id SET DEFAULT nextval('public.x_test_type_id_seq'::regclass);
 =   ALTER TABLE public.x_test_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    272    273    273            �           2604    25630    x_time_period id    DEFAULT     t   ALTER TABLE ONLY public.x_time_period ALTER COLUMN id SET DEFAULT nextval('public.x_time_period_id_seq'::regclass);
 ?   ALTER TABLE public.x_time_period ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    261    260    261            �           2604    25792    x_timesheet id    DEFAULT     p   ALTER TABLE ONLY public.x_timesheet ALTER COLUMN id SET DEFAULT nextval('public.x_timesheet_id_seq'::regclass);
 =   ALTER TABLE public.x_timesheet ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    300    301    301            �           2604    25800    x_timesheet_assessment id    DEFAULT     �   ALTER TABLE ONLY public.x_timesheet_assessment ALTER COLUMN id SET DEFAULT nextval('public.x_timesheet_assessment_id_seq'::regclass);
 H   ALTER TABLE public.x_timesheet_assessment ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    302    303    303            �           2604    25726    x_training id    DEFAULT     n   ALTER TABLE ONLY public.x_training ALTER COLUMN id SET DEFAULT nextval('public.x_training_id_seq'::regclass);
 <   ALTER TABLE public.x_training ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    284    285    285            �           2604    25734    x_training_organizer id    DEFAULT     �   ALTER TABLE ONLY public.x_training_organizer ALTER COLUMN id SET DEFAULT nextval('public.x_training_organizer_id_seq'::regclass);
 F   ALTER TABLE public.x_training_organizer ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    287    286    287            �           2604    25742    x_training_type id    DEFAULT     x   ALTER TABLE ONLY public.x_training_type ALTER COLUMN id SET DEFAULT nextval('public.x_training_type_id_seq'::regclass);
 A   ALTER TABLE public.x_training_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    288    289    289            �           2604    25611    x_undangan id    DEFAULT     n   ALTER TABLE ONLY public.x_undangan ALTER COLUMN id SET DEFAULT nextval('public.x_undangan_id_seq'::regclass);
 <   ALTER TABLE public.x_undangan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    256    257    257            �           2604    25619    x_undangan_detail id    DEFAULT     |   ALTER TABLE ONLY public.x_undangan_detail ALTER COLUMN id SET DEFAULT nextval('public.x_undangan_detail_id_seq'::regclass);
 C   ALTER TABLE public.x_undangan_detail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    258    259    259            �           2604    25390    x_userrole id    DEFAULT     n   ALTER TABLE ONLY public.x_userrole ALTER COLUMN id SET DEFAULT nextval('public.x_userrole_id_seq'::regclass);
 <   ALTER TABLE public.x_userrole ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    213    213            �          0    25368 
   x_addrbook 
   TABLE DATA           �   COPY public.x_addrbook (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, is_locked, attempt, email, abuid, abpwd, fp_token, fp_expired_date, fp_counter) FROM stdin;
    public          postgres    false    209          �          0    25419 	   x_address 
   TABLE DATA             COPY public.x_address (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, address1, postal_code1, rt1, rw1, kelurahan1, kecamatan1, region1, address2, postal_code2, rt2, rw2, kelurahan2, kecamatan2, region2) FROM stdin;
    public          postgres    false    221         �          0    25376 	   x_biodata 
   TABLE DATA           �  COPY public.x_biodata (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, fullname, nick_name, pob, dob, gender, religion_id, high, weight, nationality, ethnic, hobby, identity_type_id, identity_no, email, phone_number1, phone_number2, parent_phone_number, child_sequence, how_many_brothers, marital_status_id, addrbook_id, token, expired_token, marriage_year, company_id, is_process, is_complete) FROM stdin;
    public          postgres    false    211   �                0    25551    x_biodata_attachment 
   TABLE DATA           �   COPY public.x_biodata_attachment (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, file_name, file_path, notes, is_photo) FROM stdin;
    public          postgres    false    245   �                0    25562 	   x_catatan 
   TABLE DATA           �   COPY public.x_catatan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, title, note_type_id, notes) FROM stdin;
    public          postgres    false    247         4          0    25745    x_certification_type 
   TABLE DATA           �   COPY public.x_certification_type (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, code, name) FROM stdin;
    public          postgres    false    291   <      *          0    25707    x_client 
   TABLE DATA           �   COPY public.x_client (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, user_client_name, ero, user_email) FROM stdin;
    public          postgres    false    281   Y      &          0    25691 	   x_company 
   TABLE DATA           �   COPY public.x_company (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
    public          postgres    false    277                   0    25635    x_education_level 
   TABLE DATA           �   COPY public.x_education_level (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
    public          postgres    false    263   $      (          0    25699 
   x_employee 
   TABLE DATA           �   COPY public.x_employee (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, is_idle, is_ero, is_user_client, ero_email) FROM stdin;
    public          postgres    false    279   �      8          0    25759    x_employee_leave 
   TABLE DATA           �   COPY public.x_employee_leave (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, employee_id, regular_quota, annual_collective_leave, leave_already_taken) FROM stdin;
    public          postgres    false    295   �      6          0    25751    x_employee_training 
   TABLE DATA           �   COPY public.x_employee_training (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, employee_id, training_id, training_organizer_id, training_date, training_type_id, certification_type_id, status) FROM stdin;
    public          postgres    false    293   �                0    25651    x_family_relation 
   TABLE DATA           �   COPY public.x_family_relation (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description, family_tree_type_id) FROM stdin;
    public          postgres    false    267                   0    25643    x_family_tree_type 
   TABLE DATA           �   COPY public.x_family_tree_type (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
    public          postgres    false    265   *      �          0    25403    x_identity_type 
   TABLE DATA           �   COPY public.x_identity_type (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
    public          postgres    false    217   G                0    25540 
   x_keahlian 
   TABLE DATA           �   COPY public.x_keahlian (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, skill_name, skill_level_id, notes) FROM stdin;
    public          postgres    false    243   �                0    25529 
   x_keluarga 
   TABLE DATA           �   COPY public.x_keluarga (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, family_tree_type_id, family_relation_id, name, gender, dob, education_level_id, job, notes) FROM stdin;
    public          postgres    false    241   2      �          0    25496    x_keterangan_tambahan 
   TABLE DATA           �  COPY public.x_keterangan_tambahan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, emergency_contact_name, emergency_contact_phone, expected_salary, is_negotiable, start_working, is_ready_to_outoftown, is_apply_other_place, apply_place, selection_phase, is_ever_badly_sick, disease_name, disease_time, is_ever_psychotest, psychotest_needs, psychotest_time, requirementes_required, other_notes) FROM stdin;
    public          postgres    false    235   O      :          0    25767    x_leave_name 
   TABLE DATA           �   COPY public.x_leave_name (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, leave_name_id, s_start, e_end, reason, leave_contact, leave_address) FROM stdin;
    public          postgres    false    297   l      <          0    25778    x_leave_request 
   TABLE DATA           �   COPY public.x_leave_request (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, s_start, e_end, reason, leave_contact, leave_address) FROM stdin;
    public          postgres    false    299   �      �          0    25411    x_marital_status 
   TABLE DATA           �   COPY public.x_marital_status (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
    public          postgres    false    219   �      �          0    25360    x_menu_access 
   TABLE DATA           �   COPY public.x_menu_access (id, menutree_id, role_id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    207   �      �          0    25352 
   x_menutree 
   TABLE DATA           �   COPY public.x_menutree (id, title, menu_image_url, menu_icon, menu_order, menu_level, menu_parent, menu_url, menu_type, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    205                    0    25667    x_note_type 
   TABLE DATA           �   COPY public.x_note_type (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
    public          postgres    false    271   2      
          0    25573    x_online_test 
   TABLE DATA           �   COPY public.x_online_test (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, period_code, period, test_date, expired_test, user_access, status) FROM stdin;
    public          postgres    false    249   O                0    25581    x_online_test_detail 
   TABLE DATA           �   COPY public.x_online_test_detail (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, online_test_id, test_type_id, test_order) FROM stdin;
    public          postgres    false    251   l                 0    25518    x_organisasi 
   TABLE DATA           �   COPY public.x_organisasi (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, name, "position", entry_year, exit_year, responsibility, notes) FROM stdin;
    public          postgres    false    239   �      �          0    25485    x_pe_referensi 
   TABLE DATA           �   COPY public.x_pe_referensi (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, name, "position", address_phone, relation) FROM stdin;
    public          postgres    false    233   �      ,          0    25715    x_placement 
   TABLE DATA           �   COPY public.x_placement (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, client_id, employee_id, is_placement_active) FROM stdin;
    public          postgres    false    283   �      �          0    25395 
   x_religion 
   TABLE DATA           �   COPY public.x_religion (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
    public          postgres    false    215   �                0    25589    x_rencana_jadwal 
   TABLE DATA           	  COPY public.x_rencana_jadwal (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, schedule_code, schedule_date, "time", ro, tro, schedule_type_id, location, other_ro_tro, notes, is_automatic_mail, sent_date, status) FROM stdin;
    public          postgres    false    253   U                0    25600    x_rencana_jadwal_detail 
   TABLE DATA           �   COPY public.x_rencana_jadwal_detail (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, rencana_jadwal_id, biodata_id) FROM stdin;
    public          postgres    false    255   r      B          0    25840    x_resource_project 
   TABLE DATA           +  COPY public.x_resource_project (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, client_id, location, department, pic_name, project_name, start_project, end_project, project_role, project_phase, project_description, project_technology, main_task) FROM stdin;
    public          postgres    false    305   �      �          0    25430    x_riwayat_pekerjaan 
   TABLE DATA           &  COPY public.x_riwayat_pekerjaan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, company_name, city, country, join_year, join_month, resign_year, resign_month, last_position, income, is_it_related, about_job, exit_reason, notes) FROM stdin;
    public          postgres    false    223   �
      �          0    25463    x_riwayat_pelatihan 
   TABLE DATA             COPY public.x_riwayat_pelatihan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, training_name, organizer, training_year, training_month, training_duration, time_period_id, city, country, notes) FROM stdin;
    public          postgres    false    229   ?      �          0    25452    x_riwayat_pendidikan 
   TABLE DATA             COPY public.x_riwayat_pendidikan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, school_name, city, country, education_level_id, entry_year, graduation_year, major, gpa, notes, orders, judul_ta, deskripsi_ta) FROM stdin;
    public          postgres    false    227   �      �          0    25441    x_riwayat_proyek 
   TABLE DATA           	  COPY public.x_riwayat_proyek (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, riwayat_pekerjaan_id, start_year, start_month, project_name, project_duration, time_period_id, client, project_position, description) FROM stdin;
    public          postgres    false    225   �      �          0    25344    x_role 
   TABLE DATA           �   COPY public.x_role (id, code, name, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete) FROM stdin;
    public          postgres    false    203   �      $          0    25683    x_schedule_type 
   TABLE DATA           �   COPY public.x_schedule_type (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
    public          postgres    false    275   �      �          0    25474    x_sertifikasi 
   TABLE DATA           �   COPY public.x_sertifikasi (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, certificate_name, publisher, valid_start_year, valid_start_month, until_year, until_month, notes) FROM stdin;
    public          postgres    false    231   �                0    25659    x_skill_level 
   TABLE DATA           �   COPY public.x_skill_level (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
    public          postgres    false    269   �      �          0    25507    x_sumber_loker 
   TABLE DATA           #  COPY public.x_sumber_loker (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, biodata_id, vacancy_source, candidate_type, event_name, career_center_name, referrer_name, referrer_phone, referrer_email, other_source, last_income, apply_date) FROM stdin;
    public          postgres    false    237   �      "          0    25675    x_test_type 
   TABLE DATA           �   COPY public.x_test_type (id, created_by, create_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, description) FROM stdin;
    public          postgres    false    273   �                0    25627    x_time_period 
   TABLE DATA           �   COPY public.x_time_period (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, description) FROM stdin;
    public          postgres    false    261   �      >          0    25789    x_timesheet 
   TABLE DATA           '  COPY public.x_timesheet (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, status, placement_id, timesheet_date, s_start, e_end, overtime, start_ot, end_ot, activity, user_approval, submitted_on, approved_on, ero_status, sent_on, collected_on) FROM stdin;
    public          postgres    false    301   S      @          0    25797    x_timesheet_assessment 
   TABLE DATA           �   COPY public.x_timesheet_assessment (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, year, month, placement_id, target_result, competency, discipline) FROM stdin;
    public          postgres    false    303   p      .          0    25723 
   x_training 
   TABLE DATA           �   COPY public.x_training (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, code, name) FROM stdin;
    public          postgres    false    285   �      0          0    25731    x_training_organizer 
   TABLE DATA           �   COPY public.x_training_organizer (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, name, notes) FROM stdin;
    public          postgres    false    287   �      2          0    25739    x_training_type 
   TABLE DATA           �   COPY public.x_training_type (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, code, name) FROM stdin;
    public          postgres    false    289   �                0    25608 
   x_undangan 
   TABLE DATA           �   COPY public.x_undangan (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, schedule_type_id, invitation_date, invitation_code, "time", ro, tro, other_ro_tro, location, status) FROM stdin;
    public          postgres    false    257   �                0    25616    x_undangan_detail 
   TABLE DATA           �   COPY public.x_undangan_detail (id, create_by, create_on, modified_by, modified_on, delete_by, delete_on, is_delete, undangan_id, biodata_id, notes) FROM stdin;
    public          postgres    false    259         �          0    25387 
   x_userrole 
   TABLE DATA           �   COPY public.x_userrole (id, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete, addrbook_id, role_id) FROM stdin;
    public          postgres    false    213         }           0    0    x_addrbook_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.x_addrbook_id_seq', 1, false);
          public          postgres    false    208            ~           0    0    x_address_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.x_address_id_seq', 2, true);
          public          postgres    false    220                       0    0    x_biodata_attachment_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.x_biodata_attachment_id_seq', 2, true);
          public          postgres    false    244            �           0    0    x_biodata_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.x_biodata_id_seq', 3, true);
          public          postgres    false    210            �           0    0    x_catatan_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.x_catatan_id_seq', 1, false);
          public          postgres    false    246            �           0    0    x_certification_type_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.x_certification_type_id_seq', 1, false);
          public          postgres    false    290            �           0    0    x_client_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.x_client_id_seq', 4, true);
          public          postgres    false    280            �           0    0    x_company_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.x_company_id_seq', 1, false);
          public          postgres    false    276            �           0    0    x_education_level_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.x_education_level_id_seq', 4, true);
          public          postgres    false    262            �           0    0    x_employee_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.x_employee_id_seq', 1, false);
          public          postgres    false    278            �           0    0    x_employee_leave_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.x_employee_leave_id_seq', 1, false);
          public          postgres    false    294            �           0    0    x_employee_training_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.x_employee_training_id_seq', 1, false);
          public          postgres    false    292            �           0    0    x_family_relation_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.x_family_relation_id_seq', 1, false);
          public          postgres    false    266            �           0    0    x_family_tree_type_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.x_family_tree_type_id_seq', 1, false);
          public          postgres    false    264            �           0    0    x_identity_type_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.x_identity_type_id_seq', 2, true);
          public          postgres    false    216            �           0    0    x_keahlian_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.x_keahlian_id_seq', 5, true);
          public          postgres    false    242            �           0    0    x_keluarga_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.x_keluarga_id_seq', 1, false);
          public          postgres    false    240            �           0    0    x_keterangan_tambahan_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.x_keterangan_tambahan_id_seq', 1, false);
          public          postgres    false    234            �           0    0    x_leave_name_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.x_leave_name_id_seq', 1, false);
          public          postgres    false    296            �           0    0    x_leave_request_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.x_leave_request_id_seq', 1, false);
          public          postgres    false    298            �           0    0    x_marital_status_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.x_marital_status_id_seq', 2, true);
          public          postgres    false    218            �           0    0    x_menu_access_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.x_menu_access_id_seq', 1, false);
          public          postgres    false    206            �           0    0    x_menutree_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.x_menutree_id_seq', 1, false);
          public          postgres    false    204            �           0    0    x_note_type_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.x_note_type_id_seq', 1, false);
          public          postgres    false    270            �           0    0    x_online_test_detail_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.x_online_test_detail_id_seq', 1, false);
          public          postgres    false    250            �           0    0    x_online_test_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.x_online_test_id_seq', 1, false);
          public          postgres    false    248            �           0    0    x_organisasi_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.x_organisasi_id_seq', 1, false);
          public          postgres    false    238            �           0    0    x_pe_referensi_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.x_pe_referensi_id_seq', 1, false);
          public          postgres    false    232            �           0    0    x_placement_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.x_placement_id_seq', 1, false);
          public          postgres    false    282            �           0    0    x_religion_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.x_religion_id_seq', 5, true);
          public          postgres    false    214            �           0    0    x_rencana_jadwal_detail_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.x_rencana_jadwal_detail_id_seq', 1, false);
          public          postgres    false    254            �           0    0    x_rencana_jadwal_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.x_rencana_jadwal_id_seq', 1, false);
          public          postgres    false    252            �           0    0    x_resource_project_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.x_resource_project_id_seq', 12, true);
          public          postgres    false    304            �           0    0    x_riwayat_pekerjaan_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.x_riwayat_pekerjaan_id_seq', 2, true);
          public          postgres    false    222            �           0    0    x_riwayat_pelatihan_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.x_riwayat_pelatihan_id_seq', 1, true);
          public          postgres    false    228            �           0    0    x_riwayat_pendidikan_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.x_riwayat_pendidikan_id_seq', 3, true);
          public          postgres    false    226            �           0    0    x_riwayat_proyek_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.x_riwayat_proyek_id_seq', 1, false);
          public          postgres    false    224            �           0    0    x_role_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.x_role_id_seq', 1, false);
          public          postgres    false    202            �           0    0    x_schedule_type_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.x_schedule_type_id_seq', 1, false);
          public          postgres    false    274            �           0    0    x_sertifikasi_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.x_sertifikasi_id_seq', 2, true);
          public          postgres    false    230            �           0    0    x_skill_level_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.x_skill_level_id_seq', 1, false);
          public          postgres    false    268            �           0    0    x_sumber_loker_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.x_sumber_loker_id_seq', 1, false);
          public          postgres    false    236            �           0    0    x_test_type_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.x_test_type_id_seq', 1, false);
          public          postgres    false    272            �           0    0    x_time_period_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.x_time_period_id_seq', 4, true);
          public          postgres    false    260            �           0    0    x_timesheet_assessment_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.x_timesheet_assessment_id_seq', 1, false);
          public          postgres    false    302            �           0    0    x_timesheet_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.x_timesheet_id_seq', 1, false);
          public          postgres    false    300            �           0    0    x_training_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.x_training_id_seq', 1, false);
          public          postgres    false    284            �           0    0    x_training_organizer_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.x_training_organizer_id_seq', 1, false);
          public          postgres    false    286            �           0    0    x_training_type_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.x_training_type_id_seq', 1, false);
          public          postgres    false    288            �           0    0    x_undangan_detail_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.x_undangan_detail_id_seq', 1, false);
          public          postgres    false    258            �           0    0    x_undangan_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.x_undangan_id_seq', 1, false);
          public          postgres    false    256            �           0    0    x_userrole_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.x_userrole_id_seq', 1, false);
          public          postgres    false    212                        2606    25373    x_addrbook x_addrbook_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_addrbook
    ADD CONSTRAINT x_addrbook_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_addrbook DROP CONSTRAINT x_addrbook_pkey;
       public            postgres    false    209                       2606    25427    x_address x_address_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.x_address
    ADD CONSTRAINT x_address_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.x_address DROP CONSTRAINT x_address_pkey;
       public            postgres    false    221            $           2606    25559 .   x_biodata_attachment x_biodata_attachment_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.x_biodata_attachment
    ADD CONSTRAINT x_biodata_attachment_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.x_biodata_attachment DROP CONSTRAINT x_biodata_attachment_pkey;
       public            postgres    false    245                       2606    25816    x_biodata x_biodata_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.x_biodata
    ADD CONSTRAINT x_biodata_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.x_biodata DROP CONSTRAINT x_biodata_pkey;
       public            postgres    false    211            &           2606    25570    x_catatan x_catatan_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.x_catatan
    ADD CONSTRAINT x_catatan_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.x_catatan DROP CONSTRAINT x_catatan_pkey;
       public            postgres    false    247            H           2606    25712    x_client x_client_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.x_client
    ADD CONSTRAINT x_client_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.x_client DROP CONSTRAINT x_client_pkey;
       public            postgres    false    281            D           2606    25696    x_company x_company_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.x_company
    ADD CONSTRAINT x_company_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.x_company DROP CONSTRAINT x_company_pkey;
       public            postgres    false    277            6           2606    25640 (   x_education_level x_education_level_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.x_education_level
    ADD CONSTRAINT x_education_level_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.x_education_level DROP CONSTRAINT x_education_level_pkey;
       public            postgres    false    263            R           2606    25764 &   x_employee_leave x_employee_leave_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.x_employee_leave
    ADD CONSTRAINT x_employee_leave_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.x_employee_leave DROP CONSTRAINT x_employee_leave_pkey;
       public            postgres    false    295            F           2606    25704    x_employee x_employee_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_employee
    ADD CONSTRAINT x_employee_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_employee DROP CONSTRAINT x_employee_pkey;
       public            postgres    false    279            P           2606    25756 ,   x_employee_training x_employee_training_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.x_employee_training
    ADD CONSTRAINT x_employee_training_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.x_employee_training DROP CONSTRAINT x_employee_training_pkey;
       public            postgres    false    293            :           2606    25656 (   x_family_relation x_family_relation_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.x_family_relation
    ADD CONSTRAINT x_family_relation_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.x_family_relation DROP CONSTRAINT x_family_relation_pkey;
       public            postgres    false    267            8           2606    25648 *   x_family_tree_type x_family_tree_type_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.x_family_tree_type
    ADD CONSTRAINT x_family_tree_type_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.x_family_tree_type DROP CONSTRAINT x_family_tree_type_pkey;
       public            postgres    false    265                       2606    25408 $   x_identity_type x_identity_type_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.x_identity_type
    ADD CONSTRAINT x_identity_type_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.x_identity_type DROP CONSTRAINT x_identity_type_pkey;
       public            postgres    false    217            "           2606    25548    x_keahlian x_keahlian_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_keahlian
    ADD CONSTRAINT x_keahlian_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_keahlian DROP CONSTRAINT x_keahlian_pkey;
       public            postgres    false    243                        2606    25537    x_keluarga x_keluarga_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_keluarga
    ADD CONSTRAINT x_keluarga_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_keluarga DROP CONSTRAINT x_keluarga_pkey;
       public            postgres    false    241                       2606    25504 0   x_keterangan_tambahan x_keterangan_tambahan_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.x_keterangan_tambahan
    ADD CONSTRAINT x_keterangan_tambahan_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.x_keterangan_tambahan DROP CONSTRAINT x_keterangan_tambahan_pkey;
       public            postgres    false    235            T           2606    25775    x_leave_name x_leave_name_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.x_leave_name
    ADD CONSTRAINT x_leave_name_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.x_leave_name DROP CONSTRAINT x_leave_name_pkey;
       public            postgres    false    297            V           2606    25786 $   x_leave_request x_leave_request_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.x_leave_request
    ADD CONSTRAINT x_leave_request_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.x_leave_request DROP CONSTRAINT x_leave_request_pkey;
       public            postgres    false    299            
           2606    25416 &   x_marital_status x_marital_status_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.x_marital_status
    ADD CONSTRAINT x_marital_status_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.x_marital_status DROP CONSTRAINT x_marital_status_pkey;
       public            postgres    false    219            �           2606    25365     x_menu_access x_menu_access_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.x_menu_access
    ADD CONSTRAINT x_menu_access_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.x_menu_access DROP CONSTRAINT x_menu_access_pkey;
       public            postgres    false    207            �           2606    25357    x_menutree x_menutree_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_menutree
    ADD CONSTRAINT x_menutree_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_menutree DROP CONSTRAINT x_menutree_pkey;
       public            postgres    false    205            >           2606    25672    x_note_type x_note_type_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.x_note_type
    ADD CONSTRAINT x_note_type_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.x_note_type DROP CONSTRAINT x_note_type_pkey;
       public            postgres    false    271            *           2606    25586 .   x_online_test_detail x_online_test_detail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.x_online_test_detail
    ADD CONSTRAINT x_online_test_detail_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.x_online_test_detail DROP CONSTRAINT x_online_test_detail_pkey;
       public            postgres    false    251            (           2606    25578     x_online_test x_online_test_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.x_online_test
    ADD CONSTRAINT x_online_test_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.x_online_test DROP CONSTRAINT x_online_test_pkey;
       public            postgres    false    249                       2606    25526    x_organisasi x_organisasi_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.x_organisasi
    ADD CONSTRAINT x_organisasi_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.x_organisasi DROP CONSTRAINT x_organisasi_pkey;
       public            postgres    false    239                       2606    25493 "   x_pe_referensi x_pe_referensi_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.x_pe_referensi
    ADD CONSTRAINT x_pe_referensi_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.x_pe_referensi DROP CONSTRAINT x_pe_referensi_pkey;
       public            postgres    false    233            J           2606    25720    x_placement x_placement_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.x_placement
    ADD CONSTRAINT x_placement_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.x_placement DROP CONSTRAINT x_placement_pkey;
       public            postgres    false    283                       2606    25400    x_religion x_religion_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_religion
    ADD CONSTRAINT x_religion_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_religion DROP CONSTRAINT x_religion_pkey;
       public            postgres    false    215            .           2606    25605 4   x_rencana_jadwal_detail x_rencana_jadwal_detail_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.x_rencana_jadwal_detail
    ADD CONSTRAINT x_rencana_jadwal_detail_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.x_rencana_jadwal_detail DROP CONSTRAINT x_rencana_jadwal_detail_pkey;
       public            postgres    false    255            ,           2606    25597 &   x_rencana_jadwal x_rencana_jadwal_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.x_rencana_jadwal
    ADD CONSTRAINT x_rencana_jadwal_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.x_rencana_jadwal DROP CONSTRAINT x_rencana_jadwal_pkey;
       public            postgres    false    253            \           2606    25848 *   x_resource_project x_resource_project_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.x_resource_project
    ADD CONSTRAINT x_resource_project_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.x_resource_project DROP CONSTRAINT x_resource_project_pkey;
       public            postgres    false    305                       2606    25438 ,   x_riwayat_pekerjaan x_riwayat_pekerjaan_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.x_riwayat_pekerjaan
    ADD CONSTRAINT x_riwayat_pekerjaan_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.x_riwayat_pekerjaan DROP CONSTRAINT x_riwayat_pekerjaan_pkey;
       public            postgres    false    223                       2606    25471 ,   x_riwayat_pelatihan x_riwayat_pelatihan_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.x_riwayat_pelatihan
    ADD CONSTRAINT x_riwayat_pelatihan_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.x_riwayat_pelatihan DROP CONSTRAINT x_riwayat_pelatihan_pkey;
       public            postgres    false    229                       2606    25460 .   x_riwayat_pendidikan x_riwayat_pendidikan_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.x_riwayat_pendidikan
    ADD CONSTRAINT x_riwayat_pendidikan_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.x_riwayat_pendidikan DROP CONSTRAINT x_riwayat_pendidikan_pkey;
       public            postgres    false    227                       2606    25449 &   x_riwayat_proyek x_riwayat_proyek_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.x_riwayat_proyek
    ADD CONSTRAINT x_riwayat_proyek_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.x_riwayat_proyek DROP CONSTRAINT x_riwayat_proyek_pkey;
       public            postgres    false    225            �           2606    25349    x_role x_role_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.x_role
    ADD CONSTRAINT x_role_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.x_role DROP CONSTRAINT x_role_pkey;
       public            postgres    false    203            B           2606    25688 $   x_schedule_type x_schedule_type_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.x_schedule_type
    ADD CONSTRAINT x_schedule_type_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.x_schedule_type DROP CONSTRAINT x_schedule_type_pkey;
       public            postgres    false    275                       2606    25482     x_sertifikasi x_sertifikasi_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.x_sertifikasi
    ADD CONSTRAINT x_sertifikasi_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.x_sertifikasi DROP CONSTRAINT x_sertifikasi_pkey;
       public            postgres    false    231            <           2606    25664     x_skill_level x_skill_level_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.x_skill_level
    ADD CONSTRAINT x_skill_level_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.x_skill_level DROP CONSTRAINT x_skill_level_pkey;
       public            postgres    false    269                       2606    25515 "   x_sumber_loker x_sumber_loker_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.x_sumber_loker
    ADD CONSTRAINT x_sumber_loker_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.x_sumber_loker DROP CONSTRAINT x_sumber_loker_pkey;
       public            postgres    false    237            @           2606    25680    x_test_type x_test_type_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.x_test_type
    ADD CONSTRAINT x_test_type_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.x_test_type DROP CONSTRAINT x_test_type_pkey;
       public            postgres    false    273            4           2606    25632     x_time_period x_time_period_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.x_time_period
    ADD CONSTRAINT x_time_period_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.x_time_period DROP CONSTRAINT x_time_period_pkey;
       public            postgres    false    261            Z           2606    25802 2   x_timesheet_assessment x_timesheet_assessment_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.x_timesheet_assessment
    ADD CONSTRAINT x_timesheet_assessment_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.x_timesheet_assessment DROP CONSTRAINT x_timesheet_assessment_pkey;
       public            postgres    false    303            X           2606    25794    x_timesheet x_timesheet_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.x_timesheet
    ADD CONSTRAINT x_timesheet_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.x_timesheet DROP CONSTRAINT x_timesheet_pkey;
       public            postgres    false    301            N           2606    25736 .   x_training_organizer x_training_organizer_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.x_training_organizer
    ADD CONSTRAINT x_training_organizer_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.x_training_organizer DROP CONSTRAINT x_training_organizer_pkey;
       public            postgres    false    287            L           2606    25728    x_training x_training_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_training
    ADD CONSTRAINT x_training_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_training DROP CONSTRAINT x_training_pkey;
       public            postgres    false    285            2           2606    25624 (   x_undangan_detail x_undangan_detail_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.x_undangan_detail
    ADD CONSTRAINT x_undangan_detail_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.x_undangan_detail DROP CONSTRAINT x_undangan_detail_pkey;
       public            postgres    false    259            0           2606    25613    x_undangan x_undangan_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_undangan
    ADD CONSTRAINT x_undangan_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_undangan DROP CONSTRAINT x_undangan_pkey;
       public            postgres    false    257                       2606    25392    x_userrole x_userrole_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.x_userrole
    ADD CONSTRAINT x_userrole_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.x_userrole DROP CONSTRAINT x_userrole_pkey;
       public            postgres    false    213            �      x������ � �      �   �   x����
�@�빧�\��KbL��`e����Dy���J�X>�e!!KΊ+���Ƿ�,q�m��&���p�K��n�5���5�؍�c�y��Q5��?sxS�����s~	
�U9I!i�����2�L_c��B�      �   �   x���Mn�0���)����1;�Ru�.r�l&���H@���IC6]T~Ϗ�ާ���Q+Ɯ�-bԳ��B�ɦ.[8�N���e���(#�t�O�e����VD0�J��\C;y��v���N�6zk���^�[7��l����֡v\U���Re]ܵE��#G#�����E�ye���/a��>bǘ�;t@�U
���P�`pz���D����(M_<�)mʖb�@+�a�e��:aK         U   x�3�4�4202�50�52V04�20 "�?J�4�tM�,Q�0���*H�L��I���M��L,MI��,�2"�c�9�8͉���� ��#�            x������ � �      4      x������ � �      *   �   x��ϱ
�0�9y���rI��I�u�S��K��jrM��*��"����'�d
d�3��<ư����*c#�m���Cq�a�Ը�Ӥ\&�%��>���ck�w�)��7Nܦ�������AH��_�{�_��i}jX�i�X�g��9�o��mO      &      x������ � �         �   x��ͻ�0���~
�@Q.�02B�.F5ih��$oO�"�Hg��o��3�t�wΓ�퍩����.�.3�]s�� !D�]�=��W}��5�o���a�LG�Wɿ~L�۶6�,��>�K�<�#r��7����A�      (      x������ � �      8      x������ � �      6      x������ � �            x������ � �            x������ � �      �   \   x�3�4�4202�50�52V04�20 "�?J��	��N,*)UI�KITH�KO�K��2"J{��/gpiQb��gUf��/HwniJ&W� Ό 5         o   x�3�4�4202�50�52V04�20 "�?J�4�JML.Q�
*�L,MI��2"R�c^JQ~f
��hL�F�ĲDNc�."u�F� ���- ��$�(��0�7F��� �uA?            x������ � �      �      x������ � �      :      x������ � �      <      x������ � �      �   B   x�3�4�4202�50�52��C�4��̼��T��Լ�̢L.#|j}S�2�38�R�RJ�b���� �]L      �      x������ � �      �      x������ � �             x������ � �      
      x������ � �            x������ � �             x������ � �      �      x������ � �      ,      x������ � �      �   e   x�3�4�4202�50�5�P04�20 "�?J����K)嬪H".#��d�$�´��;#�$?'3�˄(]E�%��%�y0m�Dis*M�H�i����� �n@            x������ � �            x������ � �      B   �  x��T�n�0=���?��Cj�-M�"�0z(���Q�6m�T`��;�k�*P1��7�%���)y����zﴡ�<ެ�'�[_[�W��P�6��,m�9��Պ�c\�B��T�U�b�?�
�������+v��=��M�[�j�{����*njA"���(�JM�C:��Ԡ�Y �C�g^���5�����h���o�dnR����3��8Z�������3-o1[@HQȔE&���).-p���i}~O뀣�G�/�Մ����n"����0��Ч����Wb̙��Ud�l��H]���qㅌC�1�I���|���p�рTz�_�I�i��$�r!9@������<�yٴ�Ա9o��'`�F	��� �)Sm�D�TvI(��~�P��0;���t�C7K�W�"�#�vK�����}?F��:���H;yN�r��t7 ��o�}���`^q�����םL������O_� �'�_      �   �   x���A�0׿�� �v�xM0���"�������[���$o7��*R��`](-�92g��|���.���&�M4�!D�ɛ��EVT��5��x@va�e�kޗEO�6&i0ф�ݤ[}p���0�"̩Mޤ�=�~'�M�K!��oR�      �   u   x�E̱
�0��������,�"�C�?�-�5����7��[�OIɋ�FB���,RQw��Ӹ-K1�3����s��J,|���O���'�@�k�bF6�5kz���ic��ι�!�      �   �   x���O�@�ϳ�b�����cb��C�e2�EZcw��ӷZѥ.����~�8L���@0����@��L�=;����ʡ��>��i���C�r�hhQz����	ۺӪ�7gt�C�aC�Z���
��.��HS����3����v2����j�c�%���]�������?�b���	!g[U7      �      x������ � �      �      x������ � �      $      x������ � �      �   �   x�3�4�4202�50�52V04�20 "�?J�4�t+��).IL�V�J,KN.�,(Q(�OO/J��M-�(ы(�,V��,)JT-I�Mlh�i`�M9+KS3�����1/�(?3dgHQbf^f^:�1�l�|��1z\\\ N�5�            x������ � �      �      x������ � �      "      x������ � �         `   x�3�4�4202�50�52V04�20 "�?J�)J����KWpJM���K-����2"Mkf^IjQnjJbI*H�1i���A�LH��X\ql� ��:�      >      x������ � �      @      x������ � �      .      x������ � �      0      x������ � �      2      x������ � �            x������ � �            x������ � �      �      x������ � �     